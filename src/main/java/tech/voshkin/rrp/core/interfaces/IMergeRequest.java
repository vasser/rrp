package tech.voshkin.rrp.core.interfaces;

import java.util.List;

public interface IMergeRequest {

	String getUrl();

	String getName();

	String getAuthor();

	Integer getUpvoteCount();

	String getBranchName();

	List<String> getLabels();

	Boolean getHasUnresolvedDiscussions();

	Boolean getHasConflicts();

}
