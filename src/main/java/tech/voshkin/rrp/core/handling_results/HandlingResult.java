package tech.voshkin.rrp.core.handling_results;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import tech.voshkin.rrp.core.interfaces.IHandlingResult;
import tech.voshkin.rrp.core.interfaces.IPage;

import java.util.List;

@Builder
@Value
public class HandlingResult implements IHandlingResult {

	String title;

	@Singular
	List<IPage> pages;

}
