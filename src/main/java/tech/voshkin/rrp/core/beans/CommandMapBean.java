package tech.voshkin.rrp.core.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.voshkin.rrp.core.commands.*;
import tech.voshkin.rrp.core.interfaces.ICommand;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CommandMapBean {

	@Bean
	public Map<String, Class<? extends ICommand>> commandMap() {
		Map<String, Class<? extends ICommand>> commandMap = new HashMap<>();
		commandMap.put("t", Total.class);
		commandMap.put("p", Ping.class);
		commandMap.put("h", Help.class);
		commandMap.put("b", Bugfix.class);
		commandMap.put("n", Likeless.class);
		commandMap.put("o", LikedOnce.class);
		commandMap.put("l", LikedTwiceAndBigger.class);
		commandMap.put("lt", LikeLessTwice.class);

		return commandMap;
	}

}
