package tech.voshkin.rrp.provider;

import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.voshkin.rrp.core.interfaces.IDataProvider;

@Configuration
@ConfigurationProperties(prefix = "data-provider")
public class DataProviderConfiguration {

	@Setter
	private String dataSource;

	@Bean
	public String dataSource() {
		return dataSource;
	}

	@Bean
	public IDataProvider dataProvider(DataProviderFactoryBean factoryBean) {
		return factoryBean.getObject();
	}

}
