package tech.voshkin.rrp.transport.console;

import org.springframework.stereotype.Service;
import tech.voshkin.rrp.core.interfaces.ITransport;

@Service("consoleTransport")
public class Transport implements ITransport {

	@Override
	public void serve() {
		System.out.println("Serving console");
	}

}
