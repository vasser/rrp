package tech.voshkin.rrp.core.interfaces;

public interface ICommand extends IHandlerAware {

	String getDescription();

	Class<? extends IHandler> getHandlerClass();

	IHandlingResult execute() throws Exception;

}
