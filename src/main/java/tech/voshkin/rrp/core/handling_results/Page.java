package tech.voshkin.rrp.core.handling_results;

import lombok.Value;
import tech.voshkin.rrp.core.interfaces.IPage;

@Value
public class Page implements IPage {

	String header;

	String body;

}
