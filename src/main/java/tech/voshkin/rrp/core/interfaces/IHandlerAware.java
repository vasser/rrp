package tech.voshkin.rrp.core.interfaces;

public interface IHandlerAware {

	void setHandler(final IHandler handler);

}
