package tech.voshkin.rrp.core.commands;

import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.handlers.TotalHandler;
import tech.voshkin.rrp.core.interfaces.IHandler;

@Component
public class Total extends AbstractCommand {

	@Override
	public String getDescription() {
		return "Сводный отчет по количеству";
	}

	@Override
	public Class<? extends IHandler> getHandlerClass() {
		return TotalHandler.class;
	}

}
