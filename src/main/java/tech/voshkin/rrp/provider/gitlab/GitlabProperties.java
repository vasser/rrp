package tech.voshkin.rrp.provider.gitlab;

import org.gitlab4j.api.GitLabApi;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "provider.gitlab")
public class GitlabProperties {

	private String accessToken;

	private String url;

	private Integer projectId;

	@Bean
	protected GitLabApi gitLabApi() {
		GitLabApi gitLabApi = new GitLabApi(url, accessToken);
		gitLabApi.setRequestTimeout(5000, 5000);
		return gitLabApi;

	}

	@Bean
	protected Integer projectId() {
		return this.projectId;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

}
