package tech.voshkin.rrp.core.utils;

public class StringUtils {

	public static String cutStringToLength(String origin, int maxLine) {
		int resultLength = Math.min(origin.length(), maxLine);
		boolean willChanged = origin.length() != resultLength;

		if (!willChanged) {
			return origin;
		}

		return origin.substring(0, resultLength);
	}

}
