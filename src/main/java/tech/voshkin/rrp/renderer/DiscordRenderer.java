package tech.voshkin.rrp.renderer;

import tech.voshkin.rrp.core.interfaces.IPage;
import tech.voshkin.rrp.core.interfaces.IRenderer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class DiscordRenderer implements IRenderer {

	protected Integer FIELD_LENGTH_LIMIT = 1024;

	public List<String> render(String message) {
		return divide(message, FIELD_LENGTH_LIMIT);
	}

	private List<String> divide(String inputText, Integer limit) {
		List<String> outPages = new ArrayList<>();

		if (inputText.length() < limit) {
			outPages.add(inputText.trim());
			return outPages;
		}

		// Если входное сообщение больше лимита
		AtomicReference<String> currentPageText = new AtomicReference<>("");

		inputText.lines().forEach(currentLine -> {
			// Если прибавить текущую строку к текущей странице, то не превысим лимит
			if (currentPageText.get().length() + currentLine.length() < limit) {
				currentPageText.set(currentPageText.get() + currentLine + "\n");

				int outLength = outPages.stream().mapToInt(String::length).sum();
				int currentPageLength = currentPageText.get().length();
				int inputLength = inputText.length();

				// При делении на страницы, мы в коне каждой страницы грохали перенос,
				// поэтому сейчас это надо учесть и прибавить к состаному размеру
				// количество
				// грохнутых переносов или просто количество страниц
				if (outLength + currentPageLength + outPages.size() >= inputLength) {
					outPages.add(currentPageText.get());
				}

				return;
			}

			outPages.add(currentPageText.get().trim());
			currentPageText.set(currentLine + "\n");

			int outLength = outPages.stream().mapToInt(String::length).sum();

			if (outLength + currentPageText.get().length() >= inputText.length()) {
				outPages.add(currentPageText.get());
			}
		});

		return outPages;
	}

}
