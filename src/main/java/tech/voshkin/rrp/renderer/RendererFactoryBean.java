package tech.voshkin.rrp.renderer;

import lombok.Setter;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.interfaces.IRenderer;
import tech.voshkin.rrp.core.interfaces.ITransport;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class RendererFactoryBean implements FactoryBean<IRenderer> {

	@Setter
	private String transportType;

	private Map<ITransport.TYPE, IRenderer> rendererMap() {
		HashMap<ITransport.TYPE, IRenderer> rendererMap = new HashMap<>();
		rendererMap.put(ITransport.TYPE.DISCORD, new DiscordRenderer());
		rendererMap.put(ITransport.TYPE.CONSOLE, new PlaintextRenderer());

		return rendererMap;
	}

	@Override
	public IRenderer getObject() {
		Optional<ITransport.TYPE> type = Arrays.stream(ITransport.TYPE.values())
				.filter(typeEl -> typeEl.name().equalsIgnoreCase(transportType)).findFirst();

		if (type.isEmpty()) {
			throw new IllegalArgumentException("Unknown transport type " + transportType);
		}

		return rendererMap().get(type.get());
	}

	@Override
	public Class<IRenderer> getObjectType() {
		return IRenderer.class;
	}

}
