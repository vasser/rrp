package tech.voshkin.rrp.renderer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class RendererFactoryBeanTest {

	@Test
	public void testCreate__DiscordRenderer() {
		var factoryBean = new RendererFactoryBean();
		factoryBean.setTransportType("discord");
		assertTrue(factoryBean.getObject() instanceof DiscordRenderer);
	}

}