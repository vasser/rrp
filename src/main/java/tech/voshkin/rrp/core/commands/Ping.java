package tech.voshkin.rrp.core.commands;

import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.handlers.PingHandler;
import tech.voshkin.rrp.core.interfaces.IHandler;

@Component
public class Ping extends AbstractCommand {

	@Override
	public String getDescription() {
		return "Понг";
	}

	@Override
	public Class<? extends IHandler> getHandlerClass() {
		return PingHandler.class;
	}

}
