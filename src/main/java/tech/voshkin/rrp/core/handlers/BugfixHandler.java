package tech.voshkin.rrp.core.handlers;

import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.handling_results.HandlingResult;
import tech.voshkin.rrp.core.handling_results.Page;
import tech.voshkin.rrp.core.interfaces.IHandlingResult;
import tech.voshkin.rrp.core.interfaces.IMergeRequest;
import tech.voshkin.rrp.core.interfaces.IPage;
import tech.voshkin.rrp.core.utils.EmojiUnicodeIUtil;
import tech.voshkin.rrp.core.utils.StringUtils;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class BugfixHandler extends AbstractHandler {

	protected String toString(List<IMergeRequest> mergeRequestList) {
		if (mergeRequestList.isEmpty()) {
			return "No one MR's";
		}

		return mergeRequestList.stream()
				.map(mr -> String.format("%s [%s](%s) by %s", renderStatus(mr),
						StringUtils.cutStringToLength(mr.getName(), 30), mr.getUrl(), mr.getAuthor()))
				.collect(Collectors.joining("\n"));
	}

	private String renderStatus(IMergeRequest mr) {

		if (mr.getUpvoteCount() > 2) {
			return EmojiUnicodeIUtil.two() + EmojiUnicodeIUtil.plus();
		}

		if (mr.getUpvoteCount() == 2) {
			return EmojiUnicodeIUtil.two() + EmojiUnicodeIUtil.thumbsUp();
		}

		if (mr.getUpvoteCount() == 1) {
			return EmojiUnicodeIUtil.one() + EmojiUnicodeIUtil.thumbsUp();
		}

		if (mr.getUpvoteCount() == 0) {
			return EmojiUnicodeIUtil.zero() + EmojiUnicodeIUtil.thumbsUp();
		}

		return "";
	}

	@Override
	public IHandlingResult handle() throws Exception {

		List<IMergeRequest> data = dataProvider.getAll();

		List<IMergeRequest> dataFiltered = data.stream().filter(mr -> mr.getLabels().contains("Bugfix"))
				.sorted(Comparator.comparing(IMergeRequest::getUpvoteCount)).collect(Collectors.toList());

		List<IPage> pages = renderer.render(toString(dataFiltered)).stream().map(s -> new Page(null, s))
				.collect(Collectors.toList());

		return HandlingResult.builder().pages(pages).title("Багфиксы").build();
	}

}
