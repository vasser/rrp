package tech.voshkin.rrp.core.handlers;

import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.handling_results.HandlingResult;
import tech.voshkin.rrp.core.handling_results.Page;
import tech.voshkin.rrp.core.interfaces.IHandlingResult;
import tech.voshkin.rrp.core.interfaces.IMergeRequest;
import tech.voshkin.rrp.core.interfaces.IPage;
import tech.voshkin.rrp.core.utils.EmojiUnicodeIUtil;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TotalHandler extends AbstractHandler {

	@Override
	public IHandlingResult handle() throws Exception {

		List<IPage> pages = renderer.render(renderReport()).stream().map(page -> new Page(null, page))
				.collect(Collectors.toList());

		return HandlingResult.builder().pages(pages).title(command.getDescription()).build();
	}

	private String renderReport() throws Exception {
		List<IMergeRequest> mergeRequests = dataProvider.getAll();
		int totalCount = mergeRequests.size();

		if (totalCount == 0) {
			return "No one MR's";
		}

		List<IMergeRequest> likedRequests = mergeRequests.stream()
				.filter(mergeRequest -> mergeRequest.getUpvoteCount() >= 2).collect(Collectors.toList());
		int likedCount = likedRequests.size();
		List<IMergeRequest> onceLikedRequests = mergeRequests.stream()
				.filter(mergeRequest -> mergeRequest.getUpvoteCount() == 1).collect(Collectors.toList());
		int onceLiked = onceLikedRequests.size();
		List<IMergeRequest> noLikedRequests = mergeRequests.stream()
				.filter(mergeRequest -> mergeRequest.getUpvoteCount() == 0).collect(Collectors.toList());
		int noLiked = noLikedRequests.size();

		return String.join("\n",
				List.of(String.format("Всего - %d", totalCount),
						String.format("%s - %d", EmojiUnicodeIUtil.thumbsUp().repeat(2), likedCount),
						String.format("%s - %d", EmojiUnicodeIUtil.thumbsUp(), onceLiked),
						String.format("%s - %d", EmojiUnicodeIUtil.zero(), noLiked)));
	}

}
