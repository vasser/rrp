package tech.voshkin.rrp.transport;

import lombok.Setter;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.interfaces.ITransport;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class TransportFactoryBean implements FactoryBean<ITransport> {

	@Setter
	private String transportType;

	private final ApplicationContext applicationContext;

	private Map<ITransport.TYPE, Class<? extends ITransport>> transportMap() {
		HashMap<ITransport.TYPE, Class<? extends ITransport>> transportMap = new HashMap<>();
		transportMap.put(ITransport.TYPE.DISCORD, tech.voshkin.rrp.transport.discord.Transport.class);
		transportMap.put(ITransport.TYPE.CONSOLE, tech.voshkin.rrp.transport.console.Transport.class);

		return transportMap;
	}

	@Inject
	public TransportFactoryBean(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	public ITransport getObject() {
		Optional<ITransport.TYPE> type = Arrays.stream(ITransport.TYPE.values())
				.filter(typeEl -> typeEl.name().equalsIgnoreCase(transportType)).findFirst();

		if (type.isEmpty()) {
			throw new IllegalArgumentException("Unknown transport type " + transportType);
		}

		return applicationContext.getBean(transportMap().get(type.get()));
	}

	@Override
	public Class<ITransport> getObjectType() {
		return ITransport.class;
	}

}
