package tech.voshkin.rrp.core.interfaces;

public interface ICommandAware {

	void setCommand(ICommand command);

}
