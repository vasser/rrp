package tech.voshkin.rrp.core.merge_request;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import tech.voshkin.rrp.core.interfaces.IMergeRequest;

import java.util.List;

@Value
@Builder
public class MergeRequestDto implements IMergeRequest {

	@NonNull
	String url;

	@NonNull
	String name;

	@NonNull
	Integer upvoteCount;

	@NonNull
	String author;

	@NonNull
	String branchName;

	@NonNull
	List<String> labels;

	@NonNull
	Boolean hasConflicts;

	@NonNull
	Boolean hasUnresolvedDiscussions;

}
