package tech.voshkin.rrp.core.interfaces;

public interface IPage {

	String getHeader();

	String getBody();

}
