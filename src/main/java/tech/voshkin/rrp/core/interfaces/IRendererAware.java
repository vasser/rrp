package tech.voshkin.rrp.core.interfaces;

public interface IRendererAware {

	void setRenderer(IRenderer renderer);

}
