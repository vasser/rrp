package tech.voshkin.rrp.core.handlers;

import lombok.Setter;
import org.springframework.lang.NonNull;
import tech.voshkin.rrp.core.handling_results.Page;
import tech.voshkin.rrp.core.interfaces.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

abstract public class AbstractHandler implements IHandler, IRendererAware, ICommandAware {

	@Setter
	@NonNull
	@Inject
	protected IDataProvider dataProvider;

	@Setter
	@NonNull
	@Inject
	protected IRenderer renderer;

	@Setter
	@NonNull
	protected ICommand command;

	protected List<Page> getPagesStackWithTitle(List<String> pages, String s) {
		List<Page> headedPagesStack = new ArrayList<>();
		headedPagesStack.add(0, new Page(s, pages.get(0)));

		pages.subList(1, pages.size()).forEach(page -> {
			headedPagesStack.add(new Page(null, page));
		});

		return headedPagesStack;
	}

}
