package tech.voshkin.rrp.core.interfaces;

public interface IHandler {

	IHandlingResult handle() throws Exception;

}
