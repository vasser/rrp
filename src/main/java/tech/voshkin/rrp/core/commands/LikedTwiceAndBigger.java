package tech.voshkin.rrp.core.commands;

import org.springframework.stereotype.Component;

@Component
public class LikedTwiceAndBigger extends AbstractLikeBasedHandler {

	@Override
	public String getDescription() {
		return "Лайков 2 и более";
	}

}
