package tech.voshkin.rrp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tech.voshkin.rrp.core.interfaces.ITransport;

import javax.inject.Inject;

@SpringBootApplication
public class RppApplication implements CommandLineRunner {

	private final Logger logger = LoggerFactory.getLogger(RppApplication.class);

	private final ITransport transport;

	@Inject
	public RppApplication(ITransport transport) {
		this.transport = transport;
	}

	public static void main(String[] args) {
		SpringApplication.run(RppApplication.class, args);
	}

	@Override
	public void run(String... args) {
		transport.serve();
	}

}
