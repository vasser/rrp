package tech.voshkin.rrp.core.handlers;

import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.commands.LikedOnce;
import tech.voshkin.rrp.core.commands.LikedTwiceAndBigger;
import tech.voshkin.rrp.core.commands.Likeless;
import tech.voshkin.rrp.core.handling_results.HandlingResult;
import tech.voshkin.rrp.core.handling_results.Page;
import tech.voshkin.rrp.core.interfaces.IHandlingResult;
import tech.voshkin.rrp.core.interfaces.IMergeRequest;
import tech.voshkin.rrp.core.merge_request.Label;
import tech.voshkin.rrp.core.utils.EmojiUnicodeIUtil;
import tech.voshkin.rrp.core.utils.StringUtils;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class LikeBasedHandler extends AbstractHandler {

	@Override
	public IHandlingResult handle() throws Exception {
		List<String> mergeRequestList = dataProvider.getAll().stream().filter(this::getUpvoteCountFilter)
				.sorted(Comparator.comparing(mergeRequest -> !mergeRequest.getHasUnresolvedDiscussions()
						&& !mergeRequest.getHasUnresolvedDiscussions()
						&& !mergeRequest.getLabels().contains(Label.TO_TEST.getRealName())))
				.sorted(Comparator
						.comparing(mergeRequest -> mergeRequest.getLabels().contains(Label.TO_TEST.getRealName())))
				.sorted(Comparator.comparing(IMergeRequest::getUpvoteCount))
				.sorted(Comparator.comparing(IMergeRequest::getHasConflicts))
				.sorted(Comparator.comparing(IMergeRequest::getHasUnresolvedDiscussions)).map(this::renderMergeRequest)
				.collect(Collectors.toList());

		HandlingResult.HandlingResultBuilder builder = HandlingResult.builder();
		builder.title(command.getDescription());

		if (mergeRequestList.isEmpty()) {
			return builder.page(new Page(null, "No one MR's")).build();
		}

		List<Page> pages = renderer.render(String.join("\n", mergeRequestList)).stream()
				.map(textPage -> new Page(null, textPage)).collect(Collectors.toList());

		return builder.pages(pages).build();
	}

	private boolean getUpvoteCountFilter(IMergeRequest mr) {

		if (command instanceof Likeless) {
			return mr.getUpvoteCount() == 0;
		}

		if (command instanceof LikedOnce) {
			return mr.getUpvoteCount() == 1;
		}

		if (command instanceof LikedTwiceAndBigger) {
			return mr.getUpvoteCount() >= 2;
		}

		return mr.getUpvoteCount() < 2;
	}

	private String renderMergeRequest(IMergeRequest mergeRequest) {
		return String.format("%s [%s](%s) by %s", getEmojiPrefix(mergeRequest),
				StringUtils.cutStringToLength(mergeRequest.getName(), 30), mergeRequest.getUrl(),
				mergeRequest.getAuthor());
	}

	private String getEmojiPrefix(IMergeRequest mergeRequest) {
		boolean hasUnresolvedDiscussions = mergeRequest.getHasUnresolvedDiscussions();
		boolean hasConflicts = mergeRequest.getHasConflicts();
		// TODO лейблы вынести в конфиг и использовать через енамы.
		boolean needTest = mergeRequest.getLabels().contains(Label.TO_TEST.getRealName());

		if (hasConflicts) {
			return EmojiUnicodeIUtil.crossedSwords();
		}

		if (hasUnresolvedDiscussions) {
			return EmojiUnicodeIUtil.discussion();
		}

		if (needTest) {
			return EmojiUnicodeIUtil.dna();
		}

		return EmojiUnicodeIUtil.checkBoxChecked();
	}

}
