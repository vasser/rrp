package tech.voshkin.rrp.core.commands;

import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.handlers.BugfixHandler;
import tech.voshkin.rrp.core.interfaces.IHandler;

@Component
public class Bugfix extends AbstractCommand {

	@Override
	public String getDescription() {
		return "Багфиксы";
	}

	@Override
	public Class<? extends IHandler> getHandlerClass() {
		return BugfixHandler.class;
	}

}
