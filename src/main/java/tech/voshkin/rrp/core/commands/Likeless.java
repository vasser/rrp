package tech.voshkin.rrp.core.commands;

import org.springframework.stereotype.Component;

@Component
public class Likeless extends AbstractLikeBasedHandler {

	@Override
	public String getDescription() {
		return "Нет ни одного лайка";
	}

}
