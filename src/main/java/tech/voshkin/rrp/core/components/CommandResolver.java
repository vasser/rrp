package tech.voshkin.rrp.core.components;

import lombok.Setter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.interfaces.ICommand;
import tech.voshkin.rrp.core.interfaces.ICommandAware;
import tech.voshkin.rrp.core.interfaces.IHandler;

import javax.inject.Inject;
import java.util.Map;

@Component
public class CommandResolver implements ApplicationContextAware, IResolver {

	final private Map<String, Class<? extends ICommand>> commandMap;

	@Setter
	private ApplicationContext applicationContext;

	@Inject
	public CommandResolver(Map<String, Class<? extends ICommand>> commandMap) {
		this.commandMap = commandMap;
	}

	public ICommand resolve(String commandShortcut) {
		Class<? extends ICommand> commandClass = commandMap.get(commandShortcut);

		if (commandClass == null) {
			return null;
		}

		ICommand commandInstance = applicationContext.getBean(commandClass);
		IHandler handlerInstance = applicationContext.getBean(commandInstance.getHandlerClass());
		commandInstance.setHandler(handlerInstance);

		if (handlerInstance instanceof ICommandAware) {
			((ICommandAware) handlerInstance).setCommand(commandInstance);
		}

		return commandInstance;
	}

}
