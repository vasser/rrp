package tech.voshkin.rrp.transport.discord;

import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import discord4j.core.object.entity.channel.MessageChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tech.voshkin.rrp.core.interfaces.ICommand;
import tech.voshkin.rrp.core.components.CommandResolver;
import tech.voshkin.rrp.core.interfaces.IHandlingResult;
import tech.voshkin.rrp.core.interfaces.ITransport;
import tech.voshkin.rrp.transport.discord.utils.ReactionUtils;

import javax.inject.Inject;

@Service("discordTransport")
public class Transport implements ITransport {

	private final Logger logger = LoggerFactory.getLogger(Transport.class);

	private final GatewayDiscordClient client;

	@Value("#{commandPrefix}")
	private final String commandPrefix;

	private final CommandResolver commandResolver;

	@Inject
	public Transport(GatewayDiscordClient client, String commandPrefix, CommandResolver commandResolver) {
		this.client = client;
		this.commandPrefix = commandPrefix;
		this.commandResolver = commandResolver;
	}

	@Override
	public void serve() {
		client.getEventDispatcher().on(ReadyEvent.class).subscribe(event -> {
			final User self = event.getSelf();
			logger.info(String.format("Logged in as %s#%s%n", self.getUsername(), self.getDiscriminator()));
		});

		client.getEventDispatcher().on(MessageCreateEvent.class).map(MessageCreateEvent::getMessage)
				.filter(message -> message.getContent().startsWith(commandPrefix)).subscribe(this::handleSubscribe);

		client.onDisconnect().block();
	}

	private void handleSubscribe(Message message) {
		MessageChannel channel = message.getChannel().block();
		assert channel != null;

		ICommand command = commandResolver
				.resolve(message.getContent().substring(commandPrefix.length()).toLowerCase());

		if (command == null) {
			ReactionUtils.stopReaction(message);
			return;
		}

		ReactionUtils.seenReaction(message);

		try {
			IHandlingResult handleResult = command.execute();

			channel.createMessage(messageCreateSpec -> messageCreateSpec.setEmbed(embedCreateSpec -> {
				embedCreateSpec.setTitle(handleResult.getTitle());
				handleResult.getPages().forEach(page -> embedCreateSpec
						.addField(page.getHeader() == null ? "\u200b" : page.getHeader(), page.getBody(), false));
			})).block();
		}
		catch (Exception e) {
			logger.error("Command handling error", e);
			ReactionUtils.bugReaction(message);
		}

	}

}
