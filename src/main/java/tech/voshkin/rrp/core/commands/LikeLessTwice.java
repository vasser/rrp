package tech.voshkin.rrp.core.commands;

import org.springframework.stereotype.Component;

@Component
public class LikeLessTwice extends AbstractLikeBasedHandler {

	@Override
	public String getDescription() {
		return "Менее двух лайков";
	}

}
