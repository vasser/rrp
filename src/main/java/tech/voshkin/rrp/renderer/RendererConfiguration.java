package tech.voshkin.rrp.renderer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.voshkin.rrp.core.interfaces.IRenderer;

import javax.inject.Inject;

@Configuration
public class RendererConfiguration {

	@Bean
	@Inject
	public IRenderer renderer(@Value("#{transportType}") String transportType, RendererFactoryBean factoryBean) {
		factoryBean.setTransportType(transportType);
		return factoryBean.getObject();
	}

}
