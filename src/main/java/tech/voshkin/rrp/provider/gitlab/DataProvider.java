package tech.voshkin.rrp.provider.gitlab;

import tech.voshkin.rrp.core.merge_request.MergeRequestDto;
import tech.voshkin.rrp.core.interfaces.IDataProvider;
import tech.voshkin.rrp.core.interfaces.IMergeRequest;
import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.MergeRequestFilter;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Component("gitlabDataProvider")
public class DataProvider implements IDataProvider {

	final private GitLabApi gitlabApi;

	final private Integer projectId;

	@Inject
	public DataProvider(GitLabApi gitlabApi, Integer projectId) {
		this.gitlabApi = gitlabApi;
		this.projectId = projectId;
	}

	@Override
	public List<IMergeRequest> getAll() throws GitLabApiException {

		return gitlabApi.getMergeRequestApi().getMergeRequestsStream(getBaseFilter()).map(this::toDto)
				.collect(Collectors.toList());
	}

	private MergeRequestFilter getBaseFilter() {
		return new MergeRequestFilter().withProjectId(this.projectId).withState(Constants.MergeRequestState.OPENED)
				.withTargetBranch("master").withWip(false);
	}

	private MergeRequestDto toDto(org.gitlab4j.api.models.MergeRequest mergeRequest) {
		return MergeRequestDto.builder().url(mergeRequest.getWebUrl()).author(mergeRequest.getAuthor().getUsername())
				.name(mergeRequest.getTitle()).upvoteCount(mergeRequest.getUpvotes())
				.branchName(mergeRequest.getSourceBranch()).labels(mergeRequest.getLabels())
				.hasConflicts(mergeRequest.getHasConflicts())
				.hasUnresolvedDiscussions(!mergeRequest.getBlockingDiscussionsResolved()).build();
	}

}
