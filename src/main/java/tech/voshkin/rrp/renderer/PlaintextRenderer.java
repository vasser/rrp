package tech.voshkin.rrp.renderer;

import tech.voshkin.rrp.core.interfaces.IRenderer;

import javax.inject.Named;
import java.util.List;

@Named("plaintextRenderer")
public class PlaintextRenderer implements IRenderer {

	@Override
	public List<String> render(String message) {
		return List.of(message);
	}

}
