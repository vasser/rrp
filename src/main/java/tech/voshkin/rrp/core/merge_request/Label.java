package tech.voshkin.rrp.core.merge_request;

import lombok.Getter;

public enum Label {

	TO_TEST("To test");

	@Getter
	private final String realName;

	Label(String realName) {
		this.realName = realName;
	}

}
