package tech.voshkin.rrp.transport.discord;

import discord4j.core.DiscordClientBuilder;
import discord4j.core.GatewayDiscordClient;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "transport.discord")
public class DiscordConfiguration {

	@Setter
	private String botToken;

	@Bean
	public GatewayDiscordClient client() {
		GatewayDiscordClient client = DiscordClientBuilder.create(this.botToken).build().login().block();

		assert client != null;

		return client;
	}

}
