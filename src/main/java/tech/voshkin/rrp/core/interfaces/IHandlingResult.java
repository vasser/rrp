package tech.voshkin.rrp.core.interfaces;

import java.util.List;

public interface IHandlingResult {

	String getTitle();

	List<? extends IPage> getPages();

}
