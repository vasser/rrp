package tech.voshkin.rrp.core.interfaces;

import java.util.List;

public interface IRenderer {

	List<String> render(String message);

}
