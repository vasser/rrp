package tech.voshkin.rrp.core.commands;

import tech.voshkin.rrp.core.handlers.LikeBasedHandler;
import tech.voshkin.rrp.core.interfaces.IHandler;

public abstract class AbstractLikeBasedHandler extends AbstractCommand {

	@Override
	public Class<? extends IHandler> getHandlerClass() {
		return LikeBasedHandler.class;
	}

}
