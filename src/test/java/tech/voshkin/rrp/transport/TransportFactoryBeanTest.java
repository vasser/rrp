package tech.voshkin.rrp.transport;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import tech.voshkin.rrp.transport.discord.Transport;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TransportFactoryBeanTest {

	@Test
	@DisplayName("Test that transport factory resolve types correctly")
	void testGetTransport__ExistsTransport() {
		ApplicationContext contextMocked = mock(ApplicationContext.class);
		TransportFactoryBean factoryBean = new TransportFactoryBean(contextMocked);
		factoryBean.setTransportType("discord");
		when(contextMocked.getBean(Transport.class)).thenReturn(mock(Transport.class));
		assertTrue(factoryBean.getObject() instanceof Transport);
	}

}