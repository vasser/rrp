package tech.voshkin.rrp.core.handlers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.handling_results.HandlingResult;
import tech.voshkin.rrp.core.interfaces.ICommand;
import tech.voshkin.rrp.core.components.CommandResolver;
import tech.voshkin.rrp.core.handling_results.Page;
import tech.voshkin.rrp.core.interfaces.IHandlingResult;
import tech.voshkin.rrp.core.utils.EmojiUnicodeIUtil;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class HelpHandler extends AbstractHandler {

	@Value("#{commandMap}")
	private Map<String, Class<? extends ICommand>> commandMap;

	@Value("#{commandPrefix}")
	private String commandPrefix;

	private final CommandResolver commandResolver;

	@Inject
	public HelpHandler(CommandResolver commandResolver) {
		this.commandResolver = commandResolver;
	}

	@Override
	public IHandlingResult handle() {

		List<Page> pages = Stream.concat(renderMap().stream(), renderLegend().stream()).collect(Collectors.toList());
		return HandlingResult.builder().title(command.getDescription()).pages(pages).build();
	}

	private List<Page> renderMap() {
		String text = commandMap.keySet().stream()
				.map(key -> String.format("%s - %s", key, commandResolver.resolve(key).getDescription()))
				.map(line -> commandPrefix + line).collect(Collectors.joining("\n"));

		return getPagesStackWithTitle(renderer.render(text), "Список команд");

	}

	private List<Page> renderLegend() {
		String text = String.join("\n",
				String.format("%s + цифра - Количество лайков (Для типизировнных отчетов)",
						EmojiUnicodeIUtil.thumbsUp()),
				EmojiUnicodeIUtil.checkBoxChecked()
						+ " - Нет конфликтов, дискуссий, не нуждается в тестировании или протестировано",
				EmojiUnicodeIUtil.discussion() + " - Есть неразрешенные дискуссии",
				EmojiUnicodeIUtil.crossedSwords() + " - Есть конфликты",
				EmojiUnicodeIUtil.dna() + " - Нуждается в тестировании");

		return getPagesStackWithTitle(renderer.render(text), "Условные знаки");
	}

}
