package tech.voshkin.rrp.core.interfaces;

public interface ITransport {

	void serve();

	enum TYPE {

		DISCORD, CONSOLE;

	}

}
