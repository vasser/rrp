package tech.voshkin.rrp.core.commands;

import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.interfaces.IHandler;
import tech.voshkin.rrp.core.handlers.HelpHandler;

@Component
public class Help extends AbstractCommand {

	@Override
	public String getDescription() {
		return "Помощь";
	}

	@Override
	public Class<? extends IHandler> getHandlerClass() {
		return HelpHandler.class;
	}

}
