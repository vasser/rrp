package tech.voshkin.rrp.core.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class EmojiUnicodeIUtil {

	public String thumbsUp() {
		return "\uD83D\uDC4D";
	}

	public String dna() {
		return "\uD83E\uDDEC";
	}

	public String checkBoxChecked() {
		return "✅";
	}

	public String discussion() {
		return "\uD83D\uDDEF️";

	}

	public String crossedSwords() {
		return "⚔";
	}

	public String up() {
		return "⬆️";
	}

	public String plus() {
		return "➕";
	}

	public String two() {
		return "2️⃣";
	}

	public String one() {
		return "1️⃣";
	}

	public String zero() {
		return "0️⃣";
	}

}
