package tech.voshkin.rrp.transport.discord.utils;

import discord4j.core.object.entity.Message;
import discord4j.core.object.reaction.ReactionEmoji;

public class ReactionUtils {

	public static void addReaction(Message message, String unicode) {
		message.addReaction(ReactionEmoji.unicode(unicode)).block();
	}

	public static void seenReaction(Message message) {
		ReactionUtils.addReaction(message, "👀");
	}

	public static void stopReaction(Message message) {
		ReactionUtils.addReaction(message, "⛔");
	}

	public static void bugReaction(Message message) {
		ReactionUtils.addReaction(message, "🐞");
	}

}
