package tech.voshkin.rrp.core.components;

@FunctionalInterface
public interface IResolver {

	Object resolve(String name) throws Exception;

}
