package tech.voshkin.rrp.core;

import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "core")
public class CoreProperties {

	@Setter
	private String commandPrefix;

	@Bean
	public String commandPrefix() {
		return commandPrefix;
	}

}
