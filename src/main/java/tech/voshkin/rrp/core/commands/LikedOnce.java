package tech.voshkin.rrp.core.commands;

import org.springframework.stereotype.Component;

@Component
public class LikedOnce extends AbstractLikeBasedHandler {

	@Override
	public String getDescription() {
		return "Есть один лайк";
	}

}
