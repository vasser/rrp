package tech.voshkin.rrp.transport;

import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.voshkin.rrp.core.interfaces.ITransport;

import javax.inject.Inject;

@Configuration
@ConfigurationProperties(prefix = "transport")
public class TransportConfig {

	@Setter
	private String type;

	@Bean
	public String transportType() {
		return type;
	}

	@Bean
	@Inject
	public ITransport transport(TransportFactoryBean factoryBean) {
		factoryBean.setTransportType(transportType());
		return factoryBean.getObject();
	}

}
