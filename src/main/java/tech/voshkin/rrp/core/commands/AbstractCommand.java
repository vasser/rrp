package tech.voshkin.rrp.core.commands;

import lombok.NonNull;
import lombok.Setter;
import tech.voshkin.rrp.core.interfaces.ICommand;
import tech.voshkin.rrp.core.interfaces.IHandler;
import tech.voshkin.rrp.core.interfaces.IHandlingResult;

public abstract class AbstractCommand implements ICommand {

	@Setter
	@NonNull
	protected IHandler handler;

	public IHandlingResult execute() throws Exception {
		return this.handler.handle();
	}

}
