package tech.voshkin.rrp.provider;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.interfaces.IDataProvider;
import tech.voshkin.rrp.provider.local.DataProvider;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class DataProviderFactoryBean implements FactoryBean<IDataProvider> {

	private final String dataSource;

	private final ApplicationContext applicationContext;

	private Map<IDataProvider.SOURCE, Class<? extends IDataProvider>> providerMap() {
		Map<IDataProvider.SOURCE, Class<? extends IDataProvider>> map = new HashMap<>();
		map.put(IDataProvider.SOURCE.LOCAL, DataProvider.class);
		map.put(IDataProvider.SOURCE.GITLAB, tech.voshkin.rrp.provider.gitlab.DataProvider.class);

		return map;
	}

	@Inject
	public DataProviderFactoryBean(@Value("#{dataSource}") String dataSource, ApplicationContext applicationContext) {
		this.dataSource = dataSource;
		this.applicationContext = applicationContext;
	}

	@Override
	public IDataProvider getObject() {
		Optional<IDataProvider.SOURCE> source = Arrays.stream(IDataProvider.SOURCE.values())
				.filter(sourceEl -> sourceEl.name().equalsIgnoreCase(dataSource)).findFirst();

		if (source.isEmpty()) {
			throw new IllegalArgumentException("Unknown data source " + dataSource);
		}

		return applicationContext.getBean(providerMap().get(source.get()));

	}

	@Override
	public Class<?> getObjectType() {
		return IDataProvider.class;
	}

}
