package tech.voshkin.rrp.provider.local;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.merge_request.MergeRequestDto;
import tech.voshkin.rrp.core.interfaces.IDataProvider;
import tech.voshkin.rrp.core.interfaces.IMergeRequest;

import java.util.List;

@Component("localDataProvider")
@Profile("dev")
public class DataProvider implements IDataProvider {

	@Override
	public List<IMergeRequest> getAll() {
		return List.of(MergeRequestDto.builder().url("https://gitlab.com/project").name("Merge from local")
				.author("a.pushkin").branchName("feature_18370127").hasConflicts(true).labels(List.of("To test"))
				.hasUnresolvedDiscussions(false).upvoteCount(1).build());
	}

}
