package tech.voshkin.rrp.core.interfaces;

import java.util.List;

public interface IDataProvider {

	List<IMergeRequest> getAll() throws Exception;

	enum SOURCE {

		GITLAB, LOCAL;

	}

}
