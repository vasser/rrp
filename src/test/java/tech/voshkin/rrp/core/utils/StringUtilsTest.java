package tech.voshkin.rrp.core.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringUtilsTest {

	@Test
	@DisplayName("Test cutting for long string")
	void testCut_long() {
		String original = "Abracadabra";
		int limit = 4;
		String limitedOriginal = StringUtils.cutStringToLength(original, limit);
		assertEquals("Abra", limitedOriginal);
	}

	@Test
	@DisplayName("TEst cutting a short string (string.len < limit)")
	void testCut_short() {
		String original = "Abracadabra";
		int limit = 18;
		String limitedOriginal = StringUtils.cutStringToLength(original, limit);
		assertEquals(original, limitedOriginal);
	}

}