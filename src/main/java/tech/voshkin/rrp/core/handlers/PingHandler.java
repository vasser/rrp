package tech.voshkin.rrp.core.handlers;

import org.springframework.stereotype.Component;
import tech.voshkin.rrp.core.handling_results.HandlingResult;
import tech.voshkin.rrp.core.handling_results.Page;
import tech.voshkin.rrp.core.interfaces.IHandlingResult;

@Component
public class PingHandler extends AbstractHandler {

	@Override
	public IHandlingResult handle() {
		return HandlingResult.builder().title("Pong").page(new Page("Pong - Pong", "Pong Pong")).build();
	}

}
